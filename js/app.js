
let menu = document.querySelector("#menu");
let header= document.querySelector(".header");
let scroll__bar = document.querySelector(".scroll__bar")
menu.addEventListener("click",  (e) => {
    menu.classList.toggle("fa-times") ;
    header.classList.toggle("isActive")
});


window.onscroll= function (e) {
    menu.classList.remove("fa-times") ;
    header.classList.remove("isActive");
    scrollBarIndicator();
}


function scrollBarIndicator(){
  
  let height= window.document.body.scrollHeight - window.innerHeight;;
  let percent= (((window.scrollY)/height)*100)
  scroll__bar.style.width =  `${percent}%`
}
  